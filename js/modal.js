import { Viewer } from '@photo-sphere-viewer/core';

const hamMenu = document.querySelector(".ham-menu");
const menu = document.querySelector(".menu");

document.getElementById("open-modal-btn").addEventListener("click", openModal)
document.getElementById("close-modal-btn").addEventListener("click", closeModal)

document.getElementById("M1").addEventListener("click", () => {
    openSphereModal("../images/cam1.png")
});
document.getElementById("M2").addEventListener("click", () => {
    openSphereModal("../images/cam2.png")
});
document.getElementById("P1").addEventListener("click", () => {
    openSphereModal("../images/cam3.png")
});
document.getElementById("R2").addEventListener("click", () => {
    openSphereModal("../images/cam4.png")
});
document.getElementById("close-modal-sphere-btn").addEventListener("click", closeSphereModal)

hamMenu.addEventListener("click", () => {
    hamMenu.classList.toggle("active");
    menu.classList.toggle("active");
});

function openSphereModal(url) {
    document.getElementById("modal-sphere").classList.add("open");
    gsap.fromTo('.modal',
        {
            yPercent: -150,
            xPercent: 0,
        },
        {
            yPercent: 0,
            xPercent: 0,
            opacity: 2,
            visibility: 2,
            duration: 1,
            ease: "sine.out"
        },
    )
    setTimeout(() => {
        viewer.setPanorama(url);
    }, 1000)
}

function closeSphereModal() {
    document.getElementById("modal-sphere").classList.remove("open");
    gsap.fromTo('.modal',
        {
            yPercent: 0,
            xPercent: 0,
        },
        {
            yPercent: -300,
            xPercent: 0,
            opacity: 2,
            visibility: 2,
            duration: 1,
            ease: "sine.out"
        },
    )
}

function openModal() {
    document.getElementById("modal-form").classList.add("open");
    gsap.fromTo('.modal',
        {
            xPercent: -150,
            yPercent: 0,
        },
        {
            xPercent: 0,
            yPercent: 0,
            opacity: 2,
            visibility: 2,
            duration: 1,
            ease: "sine.out"
        },
    )
}

function closeModal() {
    document.getElementById("modal-form").classList.remove("open");
    gsap.fromTo('.modal',
        {
            xPercent: 0,
        },
        {
            xPercent: -300,
            yPercent: 0,
            opacity: 2,
            visibility: 2,
            duration: 2,
            ease: "sine.out"
        },
    )
}


const viewer = new Viewer({
    container: 'viewer',
    panorama: '../images/cam2.png',
    caption: 'Parc national du Mercantour <b>&copy; Damien Sorel</b>',
    loadingImg: 'https://photo-sphere-viewer-data.netlify.app/assets/loader.gif',
    touchmoveTwoFingers: true,
    mousewheelCtrlKey: true,
});

export default viewer;